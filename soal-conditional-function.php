<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>
    <?php 

        echo "<h3> Soal No 1 Greetings </h3>";
        /* 
            Soal No 1
            Greetings
            Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

            contoh: greetings("abduh");
            Output: "Halo Abduh, Selamat Datang di Sanbercode!"
        */

        // Code function di sini


        // Hapus komentar untuk menjalankan code!
        // greetings("Bagas");
        // greetings("Wahyu");
        // greetings("Abdul");

        echo "<br>";

        echo "<h3>Soal No 2 Calculate Multiply<h3>";

        /* 
          Soal No 2 
          Calculate Multiply 
          Buatlah sebuah fungsi bernama calculateMultiply yang mengembalikan hasil kali dua angka yang dikirim sebagai parameter.

        */
        // Code function di sini 
        
        $num1 = 2 ; 
        $num2 = 5 ;

        // Test Case (uncomment di bawah ini)
        // $hasil_kali = calculateMultiply($num1, $num2); 
        echo "Hasil kali : " ; // Isi disini dengan $hasil_kali
      
        echo "<br>";
        echo "<h3>Soal No 3 Introduction</h3>";
        /* 
          Soal No 3 
          Introduction
          Buatlah sebuah function dengan nama introduce, yang akan memproses seluruh parameter yang diinput menjadi satu kalimat perkenalan (introduction) seperti berikut: 
          "Nama saya [name],  umur saya [Age] tahun, alamat saya di [Address], dan saya punya hobby yaitu [hobby]!"

        */

        // Code Function di sini
        $name = "Agus";
        $age = 27;
        $address = "Jl. Kebun Rumput V no. 2";
        $hobby = "Mancing";
        
        // Test Case (silakan diuncomment di bawah ini)
        // $perkenalan = introduce($name, $age, $address, $hobby) 
        // echo $perkenalan;
        echo "<br>";

        echo "<h3>Soal No 4 Bandingkan Angka";
        /* 
          Soal No 4
          Bandingkan Angka
          Buatlah sebuah fungsi bandingkanAngka() yang menerima dua parameter berupa angka (number) num1 dan num2. Fungsi tersebut mengembalikan nilai boolean true jika num2 lebih besar dari num1 dan bernilai false jika num2 lebih kecil dari num1. Jika num1 sama dengan num2 maka fungsi mengembalikan nilai -1. 
        */
        function bandingkanAngka($num1, $num2) {
          // Code Function di sini
        };


        // Test Case 
        echo bandingkanAngka(5, 8) ; // true
        echo bandingkanAngka(5, 3) ; // false
        echo bandingkanAngka(4, 4) ; // -1
        echo bandingkanAngka(3, 3) ; // -1
        echo bandingkanAngka(17, 2) ; // false
        
        echo "<h3>Soal No 4 Reverse String</h3>";
        /* 
            Soal No 4
            Reverse String
            Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
            Function reverseString menerima satu parameter berupa string.
            NB: DILARANG menggunakan built-in function PHP seperti strrev(), HANYA gunakan LOOPING!

            reverseString("abdul");
            Output: ludba
            
        */
 
        // Code function di sini 


        // Hapus komentar di bawah ini untuk jalankan Code
        // reverseString("abduh");
        // reverseString("Sanbercode");
        // reverseString("We Are Sanbers Developers")
        echo "<br>";

        

        echo "<h3>Soal No 5 Palindrome </h3>";
        /* 
            Soal No 5 
            Palindrome
            Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
            Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
            Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
            NB: 
            Contoh: 
            palindrome("katak") =>  output : true
            palindrome("jambu") => output : false
            NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!
            
        */

        // Code function di sini
        
        // Hapus komentar di bawah ini untuk jalankan code
        // palindrome("civic") ; // true
        // palindrome("nababan") ; // true
        // palindrome("jambaban"); // false
        // palindrome("racecar"); // true

        echo "<br>";

        echo "<h3> Soal No 6 Konversi Menit <h3>";
        /* 
          Soal No 6
          Konversi Menit

          Diberikan sebuah function konversiMenit(menit) yang menerima satu parameter berupa angka yang merupakan ukuran waktu dalam menit. Function akan me-return string waktu dalam format jam:menit berdasarkan menit tersebut. Contoh, jika menit adalah 63, maka function akan me-return "1:03".
        */

        function konversiMenit($menit) {
          // Code di sini
        }

        // Test Cases
        echo konversiMenit(63); // 1:03
        echo konversiMenit(124); // 2:04
        echo konversiMenit(53); // 0:53
        echo konversiMenit(88); // 1:28
        echo konversiMenit(120); // 2:00

        echo "<br>";
        
        echo "<h3>Soal No 7 X dan O";
        
        /*
          Soal No 7 
          X dan O 
          Diberikan sebuah function xo(str) yang menerima satu parameter berupa string. Function akan me-return true jika jumlah karakter x sama dengan jumlah karakter o, dan false jika tidak.
        */
        function xo($str) {
          // you can only write your code here!
        }

        // Test Cases
        xo('xoxoxo'); // true
        xo('oxooxo'); // false
        xo('oxo'); // false
        xo('xxxooo'); // true
        xo('xoxooxxo'); // true
    ?>
</body>
</html>